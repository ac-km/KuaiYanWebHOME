# 用户Api 文档
## SKD各语言例子

| 语言 | 操作|详细说明 |
|:--: | :--:  |---- |
| 易语言 | [点我下载](/sdk/易语言飞鸟快验对接例子1.3.2.zip)        |易语言对接例子,账号计时,卡号计时 |
| 火山视窗 | [点我下载](/sdk/飞鸟对接火山视窗.rar)  |火山视窗对接例子,感谢群友`习惯性的回下头` 提供,非常感谢 |
| vue3.2+js | [点我下载](/sdk/vue3js调用飞鸟快验接口例子1.0.3.zip)  |vue3.2+js对接例子,其他使用js对接的都可以参考 |
| go | [点我下载](/sdk/KuaiYanGoSdk1.0.zip)  |go语言对接例子使用版本为1.20 |
| 更多例子...... |  __________  |欢迎各语言大神对接,对提供例子的大神,这里提供非竞品广告位,并给QQ群管理员,非常感谢 |
## http对接基础说明

- 所有用户Api接口都需要` TOKEN `参数放在`Header(请求协议头) `内,所以第一次请求要`取TOKEN`
- 本文档说明基于无加密明文提交,有加密就是把明文套一层加密,再提交.
- 如果需要更多功能WebApi,随时联系客服增加.

## 流程图

### 通讯无加密流程图

```state 通讯无加密流程图
state if_state <<choice>>
[*] --> 加入公共数据
加入公共数据 --> 加入接口数据 : 加入公共数据 Time (10位现行时间戳), Status(随机生成>10000)
加入接口数据 --> 最终明文数据 : 加入接口数据 Api (接口名称), 其他接口参数
最终明文数据 --> 数据组装完成 : 加密类型(无加密) 
数据组装完成 --> 响应数据 : POST请求web地址,并获取响应数据
响应数据 --> 通讯完成 : 校验公共数据(Status是否和提交相同,时间戳是否超时)没问题
响应数据 --> 根据错误代码自行处理响应数据 : 校验公共数据(Status是否和提交相同,时间戳是否超时)有问题
```

### 通讯讯MD5签名AES加密流程图

```state 通讯MD5签名AES加密流程图
state if_state <<choice>>
开始 --> 第一步 : 加入公共数据 Time (10位现行时间戳), Status(随机生成>10000)
第一步 --> 最终明文数据 : 加入接口数据 Api (接口名称), 其他接口参数
最终明文数据 -->  密文a :  base64编码(AES加密(明文,加解密密钥))
密文a -->  签名b :  MD5(密文a+AES加解密密钥)
签名b -->  数据组装完成 : <code>{"a"&#58"请求密文a","b"&#58"签名b"}</code>
数据组装完成 --> 响应数据 : POST请求web地址,并获取响应数据<code>{"a"&#58"响应密文a","b"&#58"签名b"}</code>
响应数据 -->  校验签名 :  到大写(MD5(响应数据a+AES加解密密钥))=到大写(响应数据JSON.b值)
校验签名 -->  获取明文 :  AES解密(base64解码(响应数据JSON.a值),加解密密钥)
获取明文 --> 通讯完成 : 校验公共数据(Status是否和提交相同,时间戳是否超时)没问题
获取明文 --> 根据错误代码自行处理响应数据 : 校验公共数据(Status是否和提交相同,时间戳是否超时)有问题
```

## Aes加密实际数据对比

明文 (GetToken后面空格不是必须的,只是为了api加密功能,补的空白占位,方便直接md5后替换)

```text
{"Api":"GetToken                        ","Time":1698242946,"Status":24251}
```

AES密钥`实际随机密钥不一定是文本,可能是24位字节数组`

```text
wRA0PLw87e7qkr0yhs81ZQkD
```

iv值16位空字节数组

```text
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
```

Aes cbc 192 iv模式加密
加密并把结果base64编码

```text
NX6EOre6pMajNHbhq6s2H57Bia1oG+WMWY/URdfdMKAj+LtgH6oo2dP7EuNZ9+RkfQjVoEac4YpPHRogelOb/fFSvKkoOWFqtM+OuMX1bGw=
```

在线aes测试网址[https://the-x.cn/cryptography/Aes.aspx](https://the-x.cn/cryptography/Aes.aspx)
![Alt](/images/用户api/Aes加密测试.png)

### 通讯RSA交换AES密钥混合使用流程图

```state 通讯RSA交换AES密钥混合使用流程图
state if_state <<choice>>
开始 --> 第一步 : 加入公共数据 Time (10位现行时间戳), Status(随机生成>10000)
第一步 --> 最终明文数据 : 加入接口数据 Api (接口名称), 其他接口参数
最终明文数据 -->  密文a :  base64编码(AES加密(明文,随机AES24位字节数组密钥))
密文a -->  签名b :  base64编码(公钥RSA加密(随机AES24位字节数组密钥))
签名b -->  数据组装完成 : <code>{"a"&#58"请求密文a","b"&#58"签名b实际是RSA加密AES密钥"}</code>
数据组装完成 --> 响应数据 : POST请求web地址,并获取响应数据<code>{"a"&#58"响应密文a","b"&#58"签名b实际是RSA加密AES密钥,可以通过这个长度判断,混合加密使用AES解密还是RSA解密"}</code>
响应数据 -->  RSA解密出随机AES密钥 :  公钥解密RSA(base64解码(响应数据JSON.b值))
RSA解密出随机AES密钥 -->  获取明文 :  AES解密(base64解码(响应数据JSON.a值),RSA解密出的随机AES密钥)
获取明文 --> 通讯完成 : 校验公共数据(Status是否和提交相同,时间戳是否超时)没问题
获取明文 --> 根据错误代码自行处理响应数据 : 校验公共数据(Status是否和提交相同,时间戳是否超时)有问题
```

因为需要`公钥解密` 目前已知仅OpenSSL库支持,这个库比较有名,每个语言都有这个库,可以搜索看看.

## RSA混合加密强制RSA接口列表

如果使用RSA混合加密方式,可以发包前直接搜索一下关键字,判断是否强制RSA,然后走不同的加密流程  
目前下边这几个接口,强制RSA加密通讯,其他接口可以Aes通讯提高性能

```text
	"GetToken"       
	"UserLogin"        
	"UserReduceMoney"  
	"UserReduceVipNumber"
	"UserReduceVipTime" 
	"GetVipData"        
```

## 取Token

初始化后 首先调用,获取token标志,后续所有请求都需要携带

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token:
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetToken",
    "Time": 1688007301,
    "Status": 84661
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|否|只有本接口必须为空 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Token": "ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP",
    "CryptoKeyAes": "APfsSNcyziMBa36CTRcEZGbk",
    "IP": "127.0.0.1"
  },
  "Time": 1688007301,
  "Status": 84661,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
| Token | string (文本型)|Token标志,后续所有请求协议头都需要携带 |
| CryptoKeyAes | string (文本型)|只有RSA混合加密才会返回,非强制RSA包使用该AES密钥加密通讯 |
| IP | string (文本型)|服务器显示的客户端IP |

## 登录通用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UserLogin",
    "UserOrKa": "aaaaaa",
    "PassWord": "ssssss",
    "Key": "0B4E7A0E5FE84AD35FB5F95B9CEEAC79",
    "Tab": "暂时没有动态标记",
    "AppVer": "1.0.2",
    "Time": 1688007304,
    "Status": 78453
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| UserOrKa |string (文本型)|body(提交主体JSON内)|是| 用户账号,或卡号 |
| PassWord |string (文本型)|body(提交主体JSON内)|是| 登陆密码 卡号模式空即可 |
| Key |string (文本型)|body(提交主体JSON内)|是| 绑定信息,验证绑定,如果服务器绑定为空, 绑定该值 |
| Tab |string (文本型)|body(提交主体JSON内)|是| 会显示在在线列表动态标记内,可以显示用户简单信息,可随时修改 |
| AppVer |string (文本型)|body(提交主体JSON内)|是| 当前软件版本,服务器会判断是否为可用版本,非可用版本禁止登录 |

#### 响应成功例子

```json
{
  "Data": {
    "User": "aaaaaa",
    "Key": "aaaaaa",
    "LoginIp": "127.0.0.1",
    "LoginTime": 1688007304,
    "OutUser": 0,
    "RegisterTime": 1683349292,
    "UserClassMark": 2,
    "UserClassName": "Vip2",
    "VipNumber": 115.78,
    "VipTime": 1715438220,
    "NewAppUser": false
  },
  "Time": 1688007304,
  "Status": 78453,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| User | string (文本型)|账号或卡号 |
| Key | string (文本型)|绑定信息 |
| LoginIp | string (文本型)|登录IP |
| LoginTime | int (整数型)|登录时间戳 |
| OutUser | string (文本型)|本次登录挤掉其他在线数量 |
| RegisterTime | int (整数型)|注册时间 |
| UserClassMark | int (整数型)|用户类型代号 |
| UserClassName | string (文本型)|用户类型 |
| VipNumber | float64 (双精度小数)|本应用用户积分 |
| VipTime | float64 (双精度小数)|会员到期时间戳 |
| NewAppUser | boom (逻辑型)|是否第一次登录,新用户 1.0.123版本添加|

## 用户注册
#### 请求例子
```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "NewUserInfo",
    "User": "aaaaaa",
    "PassWord": "aaaaaa",
    "Key": "绑定信息",
    "SuperPassWord": "qqqqqqq",
    "Qq": "1056795985",
    "Email": "1056795985@qq.com",
    "Phone": "13166666666",
    "Time": 1688118838,
    "Status": 87701
}
```
| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User | string (文本型)|body(提交主体JSON内)|是|要注册的用户名 |
| PassWord | string (文本型)|body(提交主体JSON内)|是|密码 |
| Key | string (文本型)|body(提交主体JSON内)|是|绑定信息 |
| SuperPassWord | string (文本型)|body(提交主体JSON内)|是|超级密码,或者可以扩展成密保问题之类的 |
| Qq |string (文本型)|body(提交主体JSON内)|否|联系QQ|
| Email | string (文本型)|body(提交主体JSON内)|否|邮箱|
| Phone |string (文本型)|body(提交主体JSON内)|否|手机号|
#### 响应成功例子
```json
{
  "Time": 1688135604,
  "Status": 87701,
  "Msg": "注册成功"
}
```
#### 响应失败例子
```json
 {
  "Time": 1684034845,
  "Status": 200,
  "Msg": "email邮箱格式不正确"
}
```
```json
 {
  "Time": 1684034898,
  "Status": 200,
  "Msg": "超级密码以字母开头，长度在6-18之间，只能包含字符、数字和下划线"
}
```
```json
 {
  "Time": 1684035056,
  "Status": 200,
  "Msg": "超级密码不能和密码相同"
}
```
```json
 {
  "Time": 1684035081,
  "Status": 200,
  "Msg": "用户已存在"
}
```
| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|

## 取用户IP
#### 请求例子
```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetUserIP",
    "Time": 1688007304,
    "Status": 78453
}
```
| 参数名 | 类型|位置|是否必须|详细说明 |
|--|----|----|----|----|
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId|
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取|
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称|
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳|
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码|

#### 响应成功例子

```json
{
  "Data": {
    "IP": "127.0.0.1"
  },
  "Time": 1688118575,
  "Status": 72085,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| IP | string (文本型)|用户IP |

## 取应用基础信息

1.0.42+版本添加可用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppInfo",
    "Time": 1688007304,
    "Status": 78453
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "AppId": 10001,
    "AppType": 1,
    "AppName": "演示对接账号限时RSA混合通讯",
    "AppWeb": "www.baidu.com"
  },
  "Time": 1688118575,
  "Status": 72085,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| AppId | int (整数型)|应用id |
| AppType | int (整数型)|应用类型 1 账号限时,2账号计点,3卡号限时,4卡号计点 |
| AppName | string (文本型)|应用名称 |
| AppWeb | string (文本型)|应用主页 |

## 用户减少余额

余额所有这个账号登录的应用都可以使用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UserReduceMoney",
    "Money": 0.01,
    "Log": "看你长得帅,减些钱",
    "AgentId": 0,
    "AgentMoney": 0,
    "AgentMoneyLog": "",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| Money |float64 (双精度小数)|body(提交主体JSON内)|是| 减少值,负数无效 |
| Log |string (文本型)|body(提交主体JSON内)|是| 减少原因,会写到日志记录|
| AgentId | int (整数型)|URL(请求URL内)|是|减少成功可以分成一定数量给代理 仅限一级代理用户id
因为二三级代理,余额完全由一级代理设置,独立全局系统金额体系外 |
| AgentMoney |float64 (双精度小数)|body(提交主体JSON内)|是| 分成金额,不能超过,减少数值, |
| AgentMoneyLog |string (文本型)|body(提交主体JSON内)|是| 分成原因,会写到余额日志记录|

#### 响应成功例子

```json
{
  "Data": {
    "Money": 0.92
  },
  "Time": 1688118839,
  "Status": 35445,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Money | float64 (双精度小数)|剩余金额 |

## 用户减少积分

积分类似余额但是只有所属应用可以使用,建议和余额1:1兑换,只想本应用使用时操作 解决计时模式时 不想要用余额又没有变量控制按次收费的问题

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UserReduceVipNumber",
    "VipNumber": 0.01,
    "Log": "看你长得帅,减些积分",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| VipNumber |float64 (双精度小数)|body(提交主体JSON内)|是| 减少值,负数无效 |
| Log |string (文本型)|body(提交主体JSON内)|是| 减少原因,会写到日志记录|

#### 响应成功例子

```json
{
  "Data": {
    "VipNumber": 0.92
  },
  "Time": 1688118839,
  "Status": 35445,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| VipNumber | float64 (双精度小数)|剩余积分 |

## 用户减少点数

只有计点方式才可以使用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UserReduceVipTime",
    "VipTime": 1,
    "Log": "看你长得帅,减些点数",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| VipTime |int (整数型)|body(提交主体JSON内)|是| 减少值,负数无效 |
| Log |string (文本型)|body(提交主体JSON内)|是| 减少原因,会写到日志记录|

#### 响应成功例子

```json
{
  "Data": {
    "VipTime": 889
  },
  "Time": 1688118839,
  "Status": 35445,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| VipTime |int (整数型)|body(提交主体JSON内)|剩余点数 |

## 取服务器连接状态

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "IsServerLink",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Time": 1688118839,
  "Status": 35445,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 取登录状态

登录状态正常返回真 异常返回假比如未登录或心跳过期,注意vip过期不会注销登录状态

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "IsLogin",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Time": 1688118839,
  "Status": 35445,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 取应用Vip数据

获取应用VIP数据,只有登录成功,且会员未过期的才可以获取

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetVipData",
    "Time": 1688118838,
    "Status": 35445
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "VipData": "这里的数据,只有登录成功并且账号会员不过期才会传输出去的数据",
    "VipData2": "这里的数据,只有登录成功并且账号会员不过期才会传输出去的数据"
  },
  "Time": 1683463084,
  "Status": 16986,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 应用VIP数据|

## 取应用公告

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppGongGao",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "AppGongGao": "我是一条公告"
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| AppGongGao | string (文本型)|应用公告内容 |

## 取专属变量

获取应用专属变量

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppPublicData",
    "Time": 1688118838,
    "Status": 87701,
    "Name": "紧急公告"
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| Name | string (文本型)|body(提交主体JSON内)|是|变量名称 |

#### 响应成功例子

```json
{
  "Data": {
    "紧急公告": "我是一条紧急公告"
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| 紧急公告 | string (文本型)|专属变量内容,具体键名已实际情况为准例子是紧急公告 |

## 取公共变量

获取公共变量

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetPublicData",
    "Name": "系统地址",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| | | | | |
| Name | string (文本型)|body(提交主体JSON内)|是|变量名称 |

#### 响应成功例子

```json
{
  "Data": {
    "系统地址": "我是一条系统地址"
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| 系统地址 | string (文本型)|公共变量内容,具体键名已实际情况为准例子是系统地址 |

## 取最新版本检测

检查成功返回真,检查失败返回假 不想使用这种格式版本号的,直接在应用专属变量,设置一个最新版本号,自己处理就好

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppVersion",
    "Version": "1.0.1",
    "IsVersionAll": true,
    "Time": 1688128499,
    "Status": 72373
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Version | string (文本型)|body(提交主体JSON内)|是|大版本号.小版本号.编译版本号   '
有可能还没登录就检测,所以还是单独传一下版本号,不使用登录时的版本号了 |
| IsVersionAll | string (文本型)|body(提交主体JSON内)|是|是否检测编译版本号,建议自动检测值为假,用户主动检测值为真 |

#### 响应成功例子

```json
{
  "Data": {
    "IsUpdate": true,
    "NewVersion": "1.1.5",
    "Version": 1.1
  },
  "Time": 1688128499,
  "Status": 72373,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| NewVersion | string (文本型)|"大版本号.小版本号.编译版本号"  版本设置第一行 用来显示 |
| Version | float64 (双精度小数)|双精度小数版本号,"大版本号.小版本号"     版本设置第一行 用来显示|
| IsUpdate | Boom (逻辑型)|true(真) 需要更新,值为false(假)不需要更新|

## 取新版本下载地址

所有软件不用登录都可以读

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUpDataJson",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "AppUpDataJson": "{\n    \"htmlurl\": \"www.baidu.com(自动下载失败打开指定网址,手动更新地址\",\n    \"data\": [{\n        \"WenJianMin\": \"文件名.exe\",\n        \"md5\": \"e10adc3949ba59abbe56e057f20f883e(小写文件md5可选,有就校验,空就只校验文件名)\",\n        \"Lujing\": \"/(下载本地相对路径)\",\n        \"size\": \"12345\",\n        \"url\": \"https://www.baidu.com/文件名.exe(下载路径)\",\n        \"YunXing\": \"1(值为更新完成后会运行这个文件,只能有一个文件值为1)\"\n\n    }, {\n        \"WenJianMin\": \"文件名.dll\",\n        \"md5\": \"e10adc3949ba59abbe56e057f20f883e(小写文件md5可选,有就校验,没有就文件名校验)\",\n        \"Lujing\": \"/(下载本地相对路径)\",\n        \"size\": \"12345\",\n        \"url\": \"https://www.baidu.com/文件名.dll(下载路径)\",\n        \"YunXing\": \"0\"\n    }]\n}"
  },
  "Time": 1688129941,
  "Status": 48837,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| AppUpDataJson | string (文本型)|下载地址数据 |

## 取应用主页Url

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppHomeUrl",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "AppHomeUrl": "www.baidu.com"
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| AppHomeUrl | string (文本型)|应用主页Url |

## 置新绑定信息

设置新的绑定信息,并按照应用设置扣时间或点数,如果原来绑定信息为空,不扣除

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetAppUserKey",
    "NewKey": "新绑定文本串",
    "User": "",
    "PassWord": "",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| NewKey | string (文本型)|body(提交主体JSON内)|是|新绑定信息 |
| User | string (文本型)|body(提交主体JSON内)|是| 可空,账号或卡号,仅用在未登录时想更换绑定 |
| PassWord | string (文本型)|body(提交主体JSON内)|是| 可空,密码如果是卡号就空即可,仅用在未登录时想更换绑定 |

#### 响应成功例子

```json
{
  "Data": {
    "ReduceVipTime": 10
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| ReduceVipTime | int (整数型)|本次换绑减少的时间或点数,如果原来绑定信息为空,不扣除 |

## 删除绑定信息

会按照设置扣点,删除掉绑定信息

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "DeleteAppUserKey",
    "User": "",
    "PassWord": "",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User | string (文本型)|body(提交主体JSON内)|是| 可空,账号或卡号,仅用在未登录时想更换绑定 |
| PassWord | string (文本型)|body(提交主体JSON内)|是| 可空,密码如果是卡号就空即可,仅用在未登录时想更换绑定 |

#### 响应成功例子

```json
{
  "Data": {
    "ReduceVipTime": 10
  },
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| ReduceVipTime | int (整数型)|本次换绑减少的时间或点数 |

## 置新用户消息

可以用来上传报错信息,用户建议,投诉等等

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetNewUserMsg",
    "MsgType": 2,
    "Msg": "内存写入错误错误信息:11191919;2424233",
    "PassWord": "",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| MsgType | string (文本型)|body(提交主体JSON内)|是|1 其他, 2 bug提交 , 3 投诉建议 |
| Msg | string (文本型)|body(提交主体JSON内)|是| 消息内容 |

#### 响应成功例子

```json
{
  "Time": 1688118839,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |

## 取验证码

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetCaptcha",
    "CaptchaType": 1,
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| CaptchaType | int (整数型)|body(提交主体JSON内)|是|验证码类型,默认 1 英数验证码 |

#### 响应成功例子

```json
{
  "Data": {
    "CaptChaImg": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAABQCAMAAAAQlwhOAAAA81BMVEUAAAApNhUdKglVYkG+y6pKVzYUIQCuu5pgbUxcaUjV4sE6RyaHlHOBjm1baEentJOzwJ9wfVyms5JdaklgbUx7iGeEkXBATSx4hWSntJNreFcoNRRreFdMWThIVTR6h2YeKwp/jGtxfl2dqok/TCucqYhLWDdyf150gWDH1LPS374xPh3S374jMA9IVTQgLQxqd1aKl3aptpVUYUB2g2JndFOPnHs6RyaVooG4xaQyPx6Sn36MmXiMmXiXpIOuu5pdakm2w6JXZENqd1aQnXxGUzIsORi3xKNtelm8yagwPRyNmnm1wqGhro1yf15jcE8iLw76KZd9AAAAAXRSTlMAQObYZgAABfJJREFUeJzsnOtO4zoQxz0Vogi1VHzZslQVAoEQiCIhFQHdFTdx+YDg/R/nqE1ie8Zje5w4tD3Lf1fntPFtfhlfxk666kc/YvSarabrbDUZbUTSp8k1vr7mIr6+zk+8sREmnk5rEDewB2s9PCzQVv2ihznt+C5tbUmJ/9ILh4eGGBTktKpNiXn/GuISzuJdaLWxH1MLVLygwMZaYM41K6BXVY+PycQlLKaCSrPZcAV43/1JNXh1vzVcYP4Mh8My4bOWrQ30XH14fw8Qp0rTGt7SueW3Yfnh81NEPKplxQVz7fnZENeq1CPQLjYX2HlKxjuKE/fu6JWLC5bYU/5LYggvB7bw76zBoBXw9hjihBa+vmoTu7QF72w2/9StW21MLm+amvFSZy6m5vmHbndB/LuZcSslnld7vOD9bRNPZBULs7WiE3+S251NtGWEeCeTKlNolFfZlqGTEy8xh6tKl/sm6km5fum/vFbDw8Q61t4SUsdZvnT9d2XDTgePxSkuAvpjEqvwhLKuHHFpKUJGDtRcoEkAO1nfCFo1s6411U7D8jq2wJYZXrCjy3Jwms1SmcUz5E0FDa002tlpRGw2Bjhc1l+AynQEdC1EtEwPEzcYzzmLUMGAmOhMBWbSjrS6rMmLtKtdq5z9n0L+tbq1cvZMIpLIytyOaKdFU4p7LwDHHuD4KdiTb2nj4rkr240hHbKAHdgNOUPYnqmd/uweg1i6vaXE/MLOGupeehEVJNVQk+cGDAYDO51kVjQ7+uoJtko5vGLXMTfm5SWFGOChYu3g2QeUCnmYWaiMvyMe9lgiyQ6KIZbVXw6ch4eHorFOp6NnVW6fi02j1oFZqaqVN3XmlfVqIMg3wsrLGA8qD6uFh3VwT+x1FiXnXLYCrsxZfD+QGaMrEdtuGr+5ERBXoZ6egwuXmBnXLDvVFZ8/0QW0DoGCg4NUYoBTWT5zd6K8Vcig8AzrrDxgrTxs/+UOohWuNZFXKXV6KiIu54hBJE9pU8/treCGWSiccnaHzi0gA7vuExaZh4sWlLWKuMnGlF6vR5OA66W6nLPIKmf/DiRk5qY1KYi02MLDnlbswFcpyotGMi2pwJ7HUI8llViIviMPoeQF9VQD1amCHlq++wZ+D1sbBzMH21F0BQukQyf79wg1m3Kr0NpgRbphWt7DAHS3pM8rFI5P7CEMuFIZ79ERNUte2ONhT05rMAY8bPbDOoSyoHAvYAa8gJgaJg2sTQHsYW8+ul1wEqvgCW2MXA+j7X6GR+BcAJlBQPevtFWwc7KD1j7eKT6O06fn4/l/zqhpde/avTfF2e+EqmG6C3DQ4/E41dLj42Olzs7OnAZr6f7eR4ynlujAY3oAPsAr79k43UbGwyplRcYKexh9C1XDTwhgTWx1jIupm7NWJhqOZPfENK29pgPdbi3iX7768JYv5mEfsfX5qoZ1c/V8Cd3kBWrO+8tD3Ed9MeZiK933tOvqqh4xje5xu+nzl4+331f2yhleQa0x7H+emd3DqsF07aqPF9FgjISWsIlSH7mMkCgbMsg9TKLjj492iMUPSphjT5GkHnYeIbTEK30Uxhxs8+LfXYp6OO8TvoXwuzRv5f9lvOdiD1tvp7mi7rVOa0bZl1r8ttTb21tC2fPzc3HeAO+UdmfQ+8vRaJTg4X1RLt7DMsl5A5pOp+6pTXVomeLh/X2buI3fCNjyzyXbsaJT7sSDe7kwJsTbwq9AbPlXi+3tKDEaw/eKf5Uy2DpzbYU9jKLpYlOZ5tq21ubW5Hg49cxhqbyXdQq1ts9LlX4xf09a4vKyFvGKqPjpxaZSe3ty4nZNalkL3s3NBA+vi/qBtM1vtEMo31GKWPMt+RrJe5QiV5Q3eDTw7WrMGxU5/Om03V7Szyn+7LZgAebttEx8d5dA/Gd3tw1iJB/vU64Glu9hkZ6eshGvif413h+ts8SvMX2jAr/mShNzWiB9UY2o0fJFX8CgCvyaK03seVA93iYLNn3FxpWEV/SvKOQ7D2rVwxLJ/52M/4v+Nd5s+i8AAP//4gVB8r31UR0AAAAASUVORK5CYII=",
    "CaptchaId": "YJZAGQXsG3uHQ4ujuNac",
    "CaptchaType": 1
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| CaptChaImg | string (文本型)|图片base64解码后如果无法显示,删除data:image/png;base64, 在解码 |
| CaptchaId |string (文本型)|验证码ID,提交验证码时一起提交 |
| CaptchaType | int (整数型)|验证码类型 1 英数验证码,2滑块验证码 |

## 取短信验证码

向手机号发送短信验证码 或 也可以填写用户名,会向用户名的手机号发送, 找回密码时可以使用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetSMSCaptcha",
    "Phone": "13188888888",
    "User": "",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Phone |string (文本型)|body(提交主体JSON内)|是|要发送短信的手机号 |
| User | string (文本型)|body(提交主体JSON内)|是|如果手机号为空 可以填写用户名,会向用户名的手机号发送,
找回密码时可以使用 |

#### 响应成功例子

```json
{
  "Data": {
    "CaptchaId": "4T7fSxvHV75tfgg",
    "CaptchaType": 3
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 应用VIP数据|
|||
| CaptchaId |string (文本型)|验证码ID,提交验证码结果时一起提交 |
| CaptchaType |string (文本型)|验证码类型 3 短信验证码 |

## 提交验证码

提交验证码没有请求,实际就是附加在需要验证码的接口json数据内即可比如例子登录需要验证码

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UserLogin",
    "UserOrKa": "aaaaaa",
    "PassWord": "s111111",
    "Key": "0B4E7A0E5FE84AD35FB5F95B9CEEAC79",
    "Tab": "暂时没有动态标记",
    "AppVer": "1.0.2",
    "Time": 1688007304,
    "Status": 87701,
    "Captcha": {
        "Type": 1,
        "Id": "YJZAGQXsG3uHQ4ujuNac",
        "Value": "abide"
    }
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
| Captcha | Object (对象)|body(提交主体JSON内)|是|如果接口需要验证码,则提交json携带该对象|
|||
| Type |int (整数型)|body(提交主体JSON内)|是|验证码类型 验证码类型 1英数验证码,2行为验证码,3短信验证码 |
| Id |string (文本型)|body(提交主体JSON内)|是|验证码ID,提交验证码结果时一起提交 |
| Value |string (文本型)|body(提交主体JSON内)|是|验证码值 |

## 取绑定信息

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUserKey",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Key": "aaaaaa"
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| Key |string (文本型)|用户绑定信息 |

## 取用户是否存在

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetIsUser",
    "User": "aaaaaa",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |string (文本型)|body(提交主体JSON内)|是|判断是否存在的用户名或卡号 |

#### 响应成功例子

```json
{
  "Data": {
    "IsUser": true
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| IsUser |boom (逻辑型)|true 已存在该用户名,false用户名不存在 卡号登录应用就是卡号已注册或未注册到应用(
卡号未使用,不会返回真)|

## 取软件用户信息

获取软件用户相关信息

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUserInfo",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Id": 1,
    "Uid": 21,
    "Key": "aaaaaa",
    "MaxOnline": 1,
    "LoginIp": "127.0.0.1",
    "LoginTime": 1683349292,
    "RegisterTime": 1683349292,
    "Status": 1,
    "User": "aaaaaa",
    "UserClassId": 22,
    "UserClassMark": 2,
    "UserClassName": "Vip2",
    "UserClassWeight": 2,
    "VipNumber": 115.78,
    "VipTime": 1715438220
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| Id | int (整数型)|应用账号的ID |
| Uid | int (整数型)|用户账号的ID,如果是卡号登录的就是卡号ID |
| Key |string (文本型)|绑定的信息|
| MaxOnline | int (整数型)|账号在本应用最大同时在线数 |
| LoginIp |string (文本型)|登录IP|
| LoginTime |int (整数型)|登录十位时间戳 |
| RegisterTime |int (整数型)|注册本应用的十位时间戳 |
| Status |int (整数型)|账号状态,1正常,2冻结 |
| User |string (文本型)|用户名 |
| UserClassId |int (整数型)|用户类型ID |
| UserClassMark |int (整数型)|用户类型代号 |
| UserClassName |string (文本型)|用户类型名称 |
| UserClassWeight |int (整数型)|用户类型权重 |
| VipNumber |float64 (双精度小数)|本应用积分 |
| VipTime |int (整数型)|会员到期十位时间戳 |

## 取用户基础信息

获取邮箱,手机号,QQ,是否已实名等账号基础信息,仅限账号登录模式可用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetUserInfo",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Id": 21,
    "Email": "1056795985@qq.com",
    "LoginAppid": 10001,
    "LoginIp": "127.0.0.1",
    "LoginTime": 1688435963,
    "Phone": "1388888888",
    "Qq": "1059795985",
    "RMB": 0.9,
    "RealNameAttestation": false,
    "RegisterIp": "113.235.144.55",
    "RegisterTime": 1519454315,
    "User": "aaaaaa"
  },
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|
|||
| Id | int (整数型)|应用账号的ID |
| Email | string (文本型)|邮箱|
| LoginAppid | int (整数型)|最后登录APPID |
| LoginIp |string (文本型)|最后登录IP|
| LoginTime | int (整数型)|最后登录十位时间戳|
| Phone |string (文本型)|手机号|
| Qq |string (文本型)|联系QQ|
| RMB |string (文本型)|账号余额|
| RealNameAttestation |boom (逻辑型)|是否已实名认证|
| RegisterIp |string (文本型)|注册IP |
| RegisterTime |int (整数型)|注册时间十位时间戳 |
| User |string (文本型)|账号用户名 |

## 置用户基础信息

设置邮箱,手机号,QQ,是否已实名等账号基础信息,仅限账号登录模式可用

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetUserQqEmailPhone",
    "Qq": "1056795985",
    "Email": "1056795985@qq.com",
    "Phone": "13166666666",
    "Time": 1688118838,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Email | string (文本型)|body(提交主体JSON内)|否|邮箱|
| Phone |string (文本型)|body(提交主体JSON内)|否|手机号|
| Qq |string (文本型)|body(提交主体JSON内)|否|联系QQ|

#### 响应成功例子

```json
{
  "Data": {},
  "Time": 1688135604,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息|

## 取系统时间戳

取服务器当前时间戳

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetSystemTime",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Time": 1684036534
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Time | int (整数型)|10位服务器当前时间戳 |

## 取软件用户备注

如果指定某个客户单独需求,可以在这里备注识别

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUserNote",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Note": "#调试权限#"
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Note | string (文本型)|软件用户备注信息 |

## 取会员到期时间戳或点数

计时模式就是到期时间戳,计点模式就是剩余点数

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUserVipTime",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "VipTime": 1714919820
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| VipTime | int (整数型)|计时模式就是到期时间戳,计点模式就是剩余点数 |

## 用户登录注销

会注销本TOKEN在线状态

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "LogOut",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |

## 用户登录注销_远程

会注销全部本应用的在线账号

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "RemoteLogOut",
    "User": "aaaaaa",
    "PassWord": "qqqqqq",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |int (整数型)|body(提交主体JSON内)|是| 用户名或卡号 |
| PassWord |int (整数型)|body(提交主体JSON内)|是| 密码,如果是卡号空即可 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |

## 心跳

更新心跳,并获取当前状态

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "HeartBeat",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Status": 1
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Status | int (整数型)|当前状态 正常返回1 会员已到期返回3(免费模式即使到期了也不会返回3) |

## 密码找回或修改_旧密码
`1.0.148`版本添加
修改成功后会注销所有在线的账号

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "OldPassWordSetPassWord",
    "User": "aaaaaa",
    "NewPassWord": "qqqqqq",
    "SuperPassWord": "wwwwww",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Type |int (整数型)|body(提交主体JSON内)|是| 找回密码方式,1使用超级密码找回 |
| User |string (文本型)|body(提交主体JSON内)|是| 用户名 |
| NewPassWord |string (文本型)|body(提交主体JSON内)|是| 新密码 |
| OldPassWord |string (文本型)|body(提交主体JSON内)|是| 旧密码 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 密码找回或修改_超级密码

修改成功后会注销所有在线的账号

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetPassWord",
    "Type": 1,
    "User": "aaaaaa",
    "NewPassWord": "qqqqqq",
    "SuperPassWord": "wwwwww",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Type |int (整数型)|body(提交主体JSON内)|是| 找回密码方式,1使用超级密码找回 |
| User |string (文本型)|body(提交主体JSON内)|是| 用户名 |
| NewPassWord |string (文本型)|body(提交主体JSON内)|是| 新密码 |
| SuperPassWord |string (文本型)|body(提交主体JSON内)|是| 超级密码 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 密码找回或修改_绑定手机

修改成功后会注销所有在线的账号,必须已经填写Phone基础信息,才能发送短信

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SmsCodeSetPassWord",
    "User": "aaaaaa",
    "NewPassWord": "qqqqqq",
    "PhoneCaptchaId": "awdadawd31135awdw",
    "PhoneCaptchaValue": "123456",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |string (文本型)|body(提交主体JSON内)|是| 用户名 |
| NewPassWord |string (文本型)|body(提交主体JSON内)|是| 新密码 |
| PhoneCaptchaId |string (文本型)|body(提交主体JSON内)|是| 短信验证码Id,调用`取短信验证码`可以获取 |
| PhoneCaptchaValue |string (文本型)|body(提交主体JSON内)|是| 收到的短信验证码 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 取用户余额

仅限账号可用,卡号不行

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetUserRmb",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Rmb": 1.11
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Rmb | float64 (双精度小数)|用户余额 |

## 取用户积分

余额和积分的区别,余额所有这个用户登录的应用都可以使用,积分只有这个用户登录的这个应用可以使用.
卡号没有余额,但是有应用积分

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAppUserVipNumber",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "VipNumber": 108.78
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| VipNumber | float64 (双精度小数)|软件积分 |

## 取开启验证码接口列表

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetCaptchaApiList",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": "{\"UserLogin\":1,\"NewUserInfo\":3,\"GetSMSCaptcha\":2}",
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Data | string (文本型)|接口列表,{"api接口名": 需要验证码类型} 英数验证码=1 行为验证码=2 短信验证码=3
例子内的为,登录接口(UserLogin)1需要英数验证码,注册接口(NewUserInfo)3需要短信验证码,,取短信验证码接口(GetSMSCaptcha)
2需要行为验证码|

## 卡号充值

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "UseKa",
    "User": "aaaaaa",
    "Ka": "S30H9i99QreDeMLmFAJkMKP6D",
    "InviteUser": "",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |string (文本型)|body(提交主体JSON内)|是|充值账号 |
| Ka |string (文本型)|body(提交主体JSON内)|是|充值卡号 |
| InviteUser |string (文本型)|body(提交主体JSON内)|否|推荐人 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

#### 响应失败例子

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "卡号已经使用到最大次数"
}
```

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "不是本应用卡号"
}
```

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "已使用本卡号充值过了,请勿重复充值"
}
```

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "用户已冻结,无法充值"
}
```

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "未注册应用,请先操作登录一次"
}
```

```json
{
  "Time": 1684072661,
  "Status": 200,
  "Msg": "用户类型不同无法充值"
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |

## 取动态标记

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetTab",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "Tab": "test测试中英文"
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Tab | string (文本型)|在线列表动态标记内容|

## 置动态标记

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetTab",
    "Tab": "test测试中英文"
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Tab | string (文本型)|body(提交主体JSON内)|是|新在线列表动态标记内容 |

#### 响应成功例子

```json
{
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |

## 余额购买积分

根据设置积分余额比例 消费余额购买积分

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayMoneyToVipNumber",
    "Money": 1.35,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Money |float64 (双精度小数)|body(提交主体JSON内)|是|要花费的金额 |

#### 响应成功例子

```json
{
  "Data": {
    "AddVipNumber": 1.35
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| AddVipNumber |float64 (双精度小数)|body(提交主体JSON内)|是|本次增加了多少积分 |

## 取可购买卡类列表

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetPayKaList",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": [
    {
      "Id": 18,
      "Money": 3,
      "Name": "天卡"
    },
    {
      "Id": 19,
      "Money": 100,
      "Name": "月卡"
    }
  ],
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Id |int (整数型)|卡类ID,余额购买充值卡时会用到该值 |
| Money |float64 (双精度小数)|销售用户售价|
| Name |string (文本型)|卡类名称 |

## 余额购买充值卡

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayMoneyToKa",
    "KaClassId": 18,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| KaClassId | int (整数型)|body(提交主体JSON内)|是|要购买的卡类id 可通过取可购买卡类列表 获取 |

#### 响应成功例子

```json
{
  "Data": {
    "AppId": 10001,
    "KaClassId": 18,
    "KaClassName": "天卡",
    "KaName": "1VBC4t1cQOf606QuTqQtGBrLV"
  },
  "Time": 1688536722,
  "Status": 63141,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| AppId |int (整数型)|卡号所属应用AppId |
| KaClassId |int (整数型)|卡号所属卡类Id |
| KaClassName |string (文本型)|卡类名称 |
| KaName |string (文本型)|卡号 |

## 取已购买卡号列表

获取历史最近购买的卡号列表

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetPurchasedKaList",
    "Number": 18,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Number | int (整数型)|body(提交主体JSON内)|是|获取最近购买的几个,推荐5 |

#### 响应成功例子

```json
{
  "Data": [
    {
      "AppId": 10001,
      "Id": 358,
      "KaClassId": 18,
      "KaClassName": "天卡",
      "Money": 3,
      "Name": "1VBC4t1cQOf606QuTqQtGBrLV",
      "Num": 0,
      "NumMax": 1,
      "RegisterTime": 1688536722,
      "Status": 1
    },
    {
      "AppId": 10001,
      "Id": 332,
      "KaClassId": 18,
      "KaClassName": "天卡",
      "Money": 3,
      "Name": "1KBzZF7YXtzHf6pDE9Qv6ecCZ",
      "Num": 1,
      "NumMax": 1,
      "RegisterTime": 1684564559,
      "Status": 1
    },
    {
      "AppId": 10001,
      "Id": 331,
      "KaClassId": 18,
      "KaClassName": "天卡",
      "Money": 3,
      "Name": "1GRAGpGtuotDYhwZCecqR8FHH",
      "Num": 1,
      "NumMax": 1,
      "RegisterTime": 1684564211,
      "Status": 1
    }
  ],
  "Time": 1688537141,
  "Status": 70701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| AppId |int (整数型)|卡号所属应用AppId |
| Id |int (整数型)|卡号Id |
| KaClassId |int (整数型)|卡号所属卡类Id |
| KaClassName |string (文本型)|卡类名称 |
| Money |float64 (双精度小数)|用户价格 |
| Name |string (文本型)|卡号 |
| Num |int (整数型)|已用次数 |
| NumMax |int (整数型)|最大可用 |
| RegisterTime |int (整数型)|制卡时间戳 |
| Status |int (整数型)|卡状态,1正常,2冻结 |

## 取用户类型列表

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetUserClassList",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": [
    {
      "Mark": 1,
      "Name": "vip1",
      "Weight": 1
    },
    {
      "Mark": 2,
      "Name": "Vip2",
      "Weight": 2
    }
  ],
  "Time": 1684376878,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Mark |int (整数型)|整数代号 |
| Name |string (文本型)|用户类型名称 |
| Weight |int (整数型)|用户类型权重 |

## 置用户类型

转换用户类型, 会根据权重切换更改时间或点数

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetUserClass",
    "Mark": 2,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Mark | int (整数型)|body(提交主体JSON内)|是|新用户类型整数代号 |

#### 响应成功例子

```json
{
  "Data": {
    "UserClassMark": 2,
    "UserClassName": "Vip2",
    "VipTime": 1699911226
  },
  "Time": 1684376878,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| UserClassMark |int (整数型)|新用户类型整数代号 |
| UserClassName |string (文本型)|新用户类型名称 |
| VipTime |int (整数型)|转换用户类型后会员到期时间戳或点数|

## 公共js函数运行

可以通过改函数拓展更多功能接口

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "RunJS",
    "Parameter": "{'a':-1.11}",
    "JsName": "用户余额增减案例",
    "IsGlobal": true,
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| JsName | string (文本型)|body(提交主体JSON内)|是|公共js函数名 |
| Parameter | string (文本型)|body(提交主体JSON内)|是|公共js形参,推荐JSON格式文本参数 |
| IsGlobal | boom (逻辑型)|body(提交主体JSON内)|是|函数归属为全局值为真,应用专属函数值为假 |

#### 响应成功例子

```json
{
  "Data": {
    "Return": {
      "IsOk": true,
      "Err": ""
    },
    "Time": 26
  },
  "Time": 1688551155,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| Return |interface (通用型)|具体返回类型以js函数返回为准,可能文本,可能对象(例子内就是),可能整数,基础类型都支持 |
| Time |int (整数型)|js函数运行耗时毫秒数,有缓存,频繁使用的js,速度会更快|

## 任务池_任务创建

当公共js函数耗时过长时就不推荐使用公共函数了,推荐使用任务池,异步并发处理,性能更高.解决长耗时功能在服务器执行,还可以通过hook函数,进行任务的控制

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "TaskPoolNewData",
    "TaskTypeId":1,
    "Parameter": "{'a':-1.11}",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| TaskTypeId | string (文本型)|body(提交主体JSON内)|是|任务类型ID |
| Parameter | string (文本型)|body(提交主体JSON内)|是|任务类型文本参数,推荐JSON格式文本参数 |

#### 响应成功例子

```json
{
  "Data": {
    "TaskUuid": "1a6547d1-269d-4ca4-b1b8-b86fb6d41287"
  },
  "Time": 1688551155,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| TaskUuid | string (文本型)|body(提交主体JSON内)|是|本次任务uuid标识,用来查询任务结果的,3秒每次轮询查询任务结果,|

## 任务池_任务查询

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "TaskPoolGetData",
    "TaskTypeId":1,
    "TaskUuid": "1a6547d1-269d-4ca4-b1b8-b86fb6d41287",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| TaskUuid | string (文本型)|body(提交主体JSON内)|是|创建任务时响应的UUID |

#### 响应成功例子

```json
{
  "Data": {
    "ReturnData": "",
    "Status": 1,
    "TimeEnd": 0,
    "TimeStart": 1684762832
  },
  "Time": 1684762832,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| ReturnData | string (文本型)|body(提交主体JSON内)|是| 任务执行结果文本,推荐JSON格式文本参数 |
| Status(Data内的) | int (整数型)|任务状态 1已创建,2任务处理中,3成功,4任务失败 其他自定义 |
| TimeEnd | int (整数型)|任务完成时间戳 |
| TimeStart | int (整数型)|任务创建时间戳 |

## 任务池_任务处理获取

仅供参考,任务池用户提交的任务,不建议用户端处理,建议服务器另开软件通过WebApi获取单独处理,保证安全性,
轮询即可已优化高性能,线程安全,推荐3秒/次

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "TaskPoolGetTask",
    "GetTaskNumber": 3,
    "GetTaskTypeId": [1,2],
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| GetTaskNumber | string (文本型)|body(提交主体JSON内)|是|获取最大数量,线程池空闲多少输入多少 |
| GetTaskTypeId | []int (数组 整数型)|body(提交主体JSON内)|是|想获取的任务类型Id |

#### 响应成功例子

```json
{
  "Data": [
    {
      "uuid": "63943989-893a-431a-b0fa-2cfb240cb782",
      "Tid": 1,
      "TimeStart": 1684766914,
      "SubmitData": "{\"a\":1}"
    },
    {
      "uuid": "8087b68b-3657-4397-9dea-599a10584b28",
      "Tid": 1,
      "TimeStart": 1684764215,
      "SubmitData": "{\"a\":1}"
    },
    {
      "uuid": "8c6d6954-00b5-40df-bf8c-ec65b995e9ea",
      "Tid": 1,
      "TimeStart": 1684767755,
      "SubmitData": "{\"a\":1}"
    }
  ],
  "Time": 1684762832,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| uuid | string (文本型)|body(提交主体JSON内)|是| 任务唯一标识uuid,提交任务结果时需要 |
| Tid | int (整数型)|任务类型id |
| TimeStart | int (整数型)|任务创建时间戳 |
| SubmitData | string (文本型)|body(提交主体JSON内)|是| 任务创建时任务类型文本参数,推荐JSON格式文本参数 |

## 任务池_任务处理返回

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "TaskPoolSetTask",
    "TaskUuid": "1a6547d1-269d-4ca4-b1b8-b86fb6d41287",
    "TaskStatus":3,
    "TaskReturnData": "{\"a\":666}",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| TaskUuid | string (文本型)|body(提交主体JSON内)|是|获取任务时响应的UUID |
| TaskStatus|int (整数型)|body(提交主体JSON内)|是| 3成功,4任务失败 其他自定义|
| TaskReturnData|string (文本型)|body(提交主体JSON内)|是|任务结果数据,推荐JSON格式文本参数 |

#### 响应成功例子

```json
{
  "Time": 1684762832,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |

## 订单_购卡直冲

无需登录,可以直接创建购卡订单, 付款后直接充值.相当于续费,

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayKaUsa",
    "User": "aaaaaa",
    "KaClassId":3,
    "PayType": "小叮当",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User | string (文本型)|body(提交主体JSON内)|是|充值用户账号或卡号 |
| KaClassId|int (整数型)|body(提交主体JSON内)|是|要购买的卡类ID|
| PayType|string (文本型)|body(提交主体JSON内)|是|支付通道"支付宝PC""微信支付""小叮当"  更多请查看系统管理->系统设置->
在线支付设置 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202307051333100001",
    "PayURL": "https://openapi.alipay.com/gateway.do?app_id=202.....后面省略",
    "PayQRCode": "wx://adadhfhjansdkaj",
    "PayQRCodePNG": "base64tupkladlwnlnadwda"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| PayURL | string (文本型)|支付类型如果要求网页支付,如支付宝pc,该值为支付链接,浏览器打开,用户手机扫码付款,否则空值|
| PayQRCode | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码原文本 否则空值|
| PayQRCodePNG | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码图片的base64编码,否则空值|

## 订单_支付购卡

无需登录,可以直接创建购卡订单, 付款后查询订单信息,会返回购买的卡号

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayGetKa",
    "KaClassId":3,
    "PayType": "小叮当",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| KaClassId|int (整数型)|body(提交主体JSON内)|是|要购买的卡类ID|
| PayType|string (文本型)|body(提交主体JSON内)|是|支付通道"支付宝PC""微信支付""小叮当"  更多请查看系统管理->系统设置->
在线支付设置 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202307051333100001",
    "PayURL": "https://openapi.alipay.com/gateway.do?app_id=202.....后面省略",
    "PayQRCode": "wx://adadhfhjansdkaj",
    "PayQRCodePNG": "base64tupkladlwnlnadwda"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| PayURL | string (文本型)|支付类型如果要求网页支付,如支付宝pc,该值为支付链接,浏览器打开,用户手机扫码付款,否则空值|
| PayQRCode | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码原文本 否则空值|
| PayQRCodePNG | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码图片的base64编码,否则空值|
## 订单_购买余额

创建购买余额, 付款后直接充值.

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayUserMoney",
    "User": "aaaaaa",
    "Money":3.11,
    "PayType": "小叮当",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User | string (文本型)|body(提交主体JSON内)|是|充值用户账号或卡号 |
| Money| float (双精度小数)|body(提交主体JSON内)|是|要充值的金额|
| PayType|string (文本型)|body(提交主体JSON内)|是|支付通道"支付宝PC""微信支付""小叮当"  更多请查看系统管理->系统设置->
在线支付设置 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202307051333100001",
    "PayURL": "https://openapi.alipay.com/gateway.do?app_id=202.....后面省略",
    "PayQRCode": "wx://adadhfhjansdkaj",
    "PayQRCodePNG": "base64tupkladlwnlnadwda"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| PayURL | string (文本型)|支付类型如果要求网页支付,如支付宝pc,该值为支付链接,浏览器打开,用户手机扫码付款,否则空值|
| PayQRCode | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码原文本 否则空值|
| PayQRCodePNG | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码图片的base64编码,否则空值|

## 余额充值_取支付通道状态

可以通过读取这,展示可用支付通道

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetPayStatus",
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |

#### 响应成功例子

```json
{
  "Data": {
    "AliPayPc": true,
    "WxPayPc": false
  },
  "Time": 1688118575,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| AliPayPc | Boom (逻辑型)|AliPayPc(支付宝PC),true可以使用,false不可使用|
| WxPayPc | Boom (逻辑型)|WxPayPc(微信支付),true可以使用,false不可使用|

## 余额充值_支付宝电脑支付
2023/11/20 已不推荐使用 推荐使用 订单_购买余额接口
返回pc网址链接,浏览器打开用户扫码付款

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetAliPayPC",
    "User": "aaaaaa",
    "Money":0.01,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |string (文本型)|body(提交主体JSON内)|是|要充值的账号 |
| Money |float64 (双精度小数)|body(提交主体JSON内)|是|要充值金额 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202307051333100001",
    "PayURL": "https://openapi.alipay.com/gateway.do?app_id=202.....后面省略"
  },
  "Time": 1688535190,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| PayURL | string (文本型)|pc支付链接,浏览器打开,用户手机扫码付款|

## 余额充值_微信电脑支付
2023/11/20 已不推荐使用 推荐使用 订单_购买余额接口
返回文本类似`weixin://wxpay/bizpayurl?pr=QDKS4KWzz`生成二维码然后扫描就可以了

#### 请求例子

```http request
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetWXPayPC",
    "User": "aaaaaa",
    "Money":0.01,
    "Time": 1688007304,
    "Status": 87701
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User |string (文本型)|body(提交主体JSON内)|是|要充值的账号 |
| Money |float64 (双精度小数)|body(提交主体JSON内)|是|要充值金额 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202305162202080001",
    "WxPayURL": "weixin://wxpay/bizpayurl?pr=QDKS4KWzz"
  },
  "Time": 1688535190,
  "Status": 87701,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|接口返回信息 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| WxPayURL | string (文本型)|微信支付文本,用这个文本生成二维码后,用户手机微信扫码付款|

## 订单_购买积分

创建购买积分, 付款后直接充值.

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "PayUserVipNumber",
    "User": "aaaaaa",
    "Money":3.11,
    "PayType": "小叮当",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| User | string (文本型)|body(提交主体JSON内)|是|充值用户账号或卡号 |
| Money| float (双精度小数)|body(提交主体JSON内)|是|要充值的金额|
| PayType|string (文本型)|body(提交主体JSON内)|是|支付通道"支付宝PC""微信支付""小叮当"  更多请查看系统管理->系统设置->
在线支付设置 |

#### 响应成功例子

```json
{
  "Data": {
    "OrderId": "202307051333100001",
    "PayURL": "https://openapi.alipay.com/gateway.do?app_id=202.....后面省略",
    "PayQRCode": "wx://adadhfhjansdkaj",
    "PayQRCodePNG": "base64tupkladlwnlnadwda"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
|||
| OrderId | string (文本型)|本地余额充值订单号,用来查询支付结果|
| PayURL | string (文本型)|支付类型如果要求网页支付,如支付宝pc,该值为支付链接,浏览器打开,用户手机扫码付款,否则空值|
| PayQRCode | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码原文本 否则空值|
| PayQRCodePNG | string (文本型)|支付类型如果要求扫码支付,如微信支付,支付宝当面付,该值为二维码图片的base64编码,否则空值|

## 订单_取订单状态查询

通过订单号查询支付结果,和对应数据

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetPayOrderStatus",
    "OrderId": "202307051333100001",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| OrderId | string (文本型)|body(提交主体JSON内)|是|订单号 |

#### 响应成功例子

```json
{
  "Data": {
    "Status": 3,
    "KaName": "aaaaasdawdawdawda"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
|||
| Status | int (整数型)| 1  '等待支付' 2  '已付待充' 3 '充值成功' 4 退款中 5 ? 退款失败" : 6退款成功|
| KaName | string (整数)| 如果为支付购卡的订单,返回卡号|

## 用户云配置_置值

向用户云配置写入值,无该值自动创建

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "SetUserConfig",
    "Name": "配置名称",
    "Value": "配置值",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Name | string (文本型)|body(提交主体JSON内)|是|云配置名称 |
| Value | string (文本型)|body(提交主体JSON内)|是|云配置值 |

#### 响应成功例子

```json
{
  "Data": {
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |

## 用户云配置_取值

取回用户云配置值,无该值返回"" 空字符串

#### 请求例子

```http request copy
POST http://127.0.0.1:18888/Api?AppId=10001 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: ALYVZWFRDF7ED72VLZMU2Q8CEHFFUKJP
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Api": "GetUserConfig",
    "Name": "配置名称",
    "Time": 1688551155,
    "Status": 13013
}
```

| 参数名 | 类型|位置|是否必须|详细说明 |
|--| ---- |---- |---- |---- |
| AppId | int (整数型)|URL(请求URL内)|是|应用AppId |
| Token | string (文本型)|Header(协议头内)|是|通过`取Token`接口获取 |
| Api | string (文本型)|body(提交主体JSON内)|是|固定接口名称 |
| Time | int (整数型)|body(提交主体JSON内)|是|10位当前时间戳 |
| Status |int (整数型)|body(提交主体JSON内)|是| 成功代码,本地随机生成>10000整数,响应成功,会返回相同代码 |
|||
| Name | string (文本型)|body(提交主体JSON内)|是|云配置名称 |

#### 响应成功例子

```json
{
  "Data": {
    "配置名称": "配置值"
  },
  "Time": 1688535190,
  "Status": 13013,
  "Msg": ""
}
```

| 参数名 | 类型|详细说明 |
|--| ---- |---- |
| Status | int (整数型)
|响应状态码,操作成功值和提交Status相同,失败的具体请查看[状态码列表](/指南/状态码查询.html#状态码列表-1) |
| msg |string (文本型)|响应状态码,提示信息 |
| Time | int (整数型)|10位服务器当前时间戳,可以用来判断封包时间 |
| Data | Object (对象)|Data内容可能会根据支付通道不同返回不同 |
| 配置名称 | string (字符串)|这个是动态变化的,传入的配置名称是什么这个键名就是什么 |



