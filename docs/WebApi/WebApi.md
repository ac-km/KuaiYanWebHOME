#  WebApi 文档

## 基础说明
- 所有WebApi接口都需要` TOKEN `参数放在`Header(请求协议头) `内或Url参数内,可以去` 用户管理->在线管理 `添加
- ` TOKEN `请给予最小接口权限,防止泄漏调用其他接口
- 建议给WebApi单独设置Host限制,不和普通用户使用相同二级域名,更安全,可以去` 系统设置->基础设置->WebApiHost `设置,
- 设置限制`WebApiHost`后,协议头一定携带 `Referer: 您的域名`参数,不然可能访问失败
- 如果需要更多功能WebApi,随时联系客服增加.
### 状态码列表
| code(响应状态码) | 详细说明  |
|--|----  |
| 10000 |请求成功 |
| 200 |请求失败,提交参数错误,参数格式不是JSON,缺少参数,参数类型不正确,等等, |
| 201 | login登录状态失效,TOKEN错误, |
## 任务池
### 任务池 获取任务信息
当二开功能耗费时间较长时不建议继续使用公共函数同步返回,推荐使用任务池方式异步处理功能, 异步队列处理,并发性能更高,充分发挥go多线程多协程优势
通过WebApi获取任务,3秒一次,轮询即可, 内部已优化,

#### 请求例子

```http request
POST http://127.0.0.1:18888/WebApi/TaskPoolGetTask HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)
Referer: http://127.0.0.1:18888

{
    "GetTaskNumber": 5,
    "GetTaskTypeId": [1, 2, 3]
}
```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/TaskPoolGetTask |
| 请求方式 | POST |


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Token | string (文本型)|是|系统在线管理内可以添加获取 |
| GetTaskNumber | int (整数型)|是|本次请求获取任务最大数量 |
| GetTaskTypeId |[]int (整数型 数组)|是| 数组获取任务类型ID |

#### 响应成功例子
```json
{
  "code": 0,
  "data": [{
    "uuid": "5514d5a3-6d57-4b5a-afad-31bc37054abd",
    "Tid": 1,
    "TimeStart": 1687060862,
    "SubmitData": "{'a':1}"
  }, {
    "uuid": "58310db4-310a-438a-ad3b-137cdcc88013",
    "Tid": 1,
    "TimeStart": 1687060863,
    "SubmitData": "{'a':1}"
  }],
  "msg": "获取成功"
}
```

| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | []Object (对象 数组)|本次请求获取任务列表 |
| uuid | string (文本型)|任务唯一ID 提交,获取,查询,任务信息,都要用 |
| Tid | int (整数型)|任务类型ID |
| TimeStart | int (整数型)|任务创建时间戳,获取到可以判断一下,如果有时间限制的任务,直接提交失败结果 |
| SubmitData | string (文本型)|任务创建时提供的任务参数 |


### 任务池 提交任务结果

#### 请求例子
```http request
POST http://127.0.0.1:18888/WebApi/TaskPoolSetTask HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)
Referer: http://127.0.0.1:18888

{
    "TaskUuid": "5514d5a3-6d57-4b5a-afad-31bc37054abd",
    "TaskStatus": 3,
    "TaskReturnData": "BB6CB5C68DF4652941CAF652A366F2D8"
}

```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/TaskPoolSetTask |
| 请求方式 | POST |


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Token | string (文本型)|是|系统在线管理内可以添加获取 |
| TaskUuid | string (文本型)|是|获取任务获取到的任务UUID |
| TaskStatus |int (整数型)|是| 3成功,4任务失败 其他自定义只要查询时可以处理就行 |
| TaskReturnData | string (文本型)|是|任务结果数据,推荐json文本,可以存储多个数据 |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": {},
  "msg": "操作成功"
}
```
#### 响应失败例子
```json
{"code":200,"data":{},"msg":"UUid错误"}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | []Object (对象 数组)|暂无用处 |
## 二开扩展
### 公共函数 执行公共Js函数
通过该WebApi可以直接执行,公共函数JS,让WebApi可以扩展更多功能
#### 请求例子
```http request
POST http://127.0.0.1:18888/WebApi/RunJs HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "JsName": "WebApi_用户Id取详情",
    "Parameter": "{'Uid':1}"
}

```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/RunJs |
| 请求方式 | POST |


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Token | string (文本型)|是|系统在线管理内可以添加获取 |
| JsName | string (文本型)|是|公共函数JS名称 |
| Parameter | string (文本型)|是|公共函数JS参数,这里是一个json格式内有UId的键名值为1, |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": {
    "Id": 1,
    "User": "feizi666",
    "Phone": "",
    "Email": ""
  },
  "msg": "运行成功,耗时:2"
}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | interface (通用型)|公共函数返回的信息,具体看公共函数js返回值是什么, 如果是对象,这里也是对象, 如果是文本,这里也是文本 |
#### 响应失败例子
```json
{"code":200,"data":{},"msg":"JS公共函数不存在"}
```
```json
{"code":200,"data":{},"msg":"异常:可能Hook函数传参或返回值类型错误,具体:SyntaxError: Unexpected token at the end: \u003cnil\u003e\n\tat parse (native)\n\tat WebApi_用户Id取详情 (\u003ceval\u003e:4:38(10))\n"}
```
```json
{"code":200,"data":{},"msg":"异常:可能Hook函数传参或返回值类型错误,具体:js引擎未返回报错信息"}
```

## 应用
### 取App最新下载地址
通过该WebApi获取应用APPID更新下载地址
#### 请求例子
```http request
GET http://127.0.0.1:18888/WebApi/GetAppUpDataJson?AppId=10001&Token=LAX6LCUN6KHBHYURTKHL2SM0QFIKMYG4 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)
```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/GetAppUpDataJson |
| 请求方式 | GET |

| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Token | string (文本型)|是|系统在线管理内可以添加获取 |
| AppId | int (整数型)|是|要获取下载地址的应用ID |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": "{\n    \"htmlurl\": \"www.baidu.com(自动下载失败打开指定网址,手动更新地址\",\n    \"data\": [{\n        \"WenJianMin\": \"文件名.exe\",\n        \"md5\": \"e10adc3949ba59abbe56e057f20f883e(小写文件md5可选,有就校验,空就只校验文件名)\",\n        \"Lujing\": \"/(下载本地相对路径)\",\n        \"size\": \"12345\",\n        \"url\": \"https://www.baidu.com/文件名.exe(下载路径)\",\n        \"YunXing\": \"1(值为更新完成后会运行这个文件,只能有一个文件值为1)\"\n\n    }, {\n        \"WenJianMin\": \"文件名.dll\",\n        \"md5\": \"e10adc3949ba59abbe56e057f20f883e(小写文件md5可选,有就校验,没有就文件名校验)\",\n        \"Lujing\": \"/(下载本地相对路径)\",\n        \"size\": \"12345\",\n        \"url\": \"https://www.baidu.com/文件名.dll(下载路径)\",\n        \"YunXing\": \"0\"\n    }]\n}",
  "msg": "获取成功"
}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | interface (通用型)|公共函数返回的信息,具体看公共函数js返回值是什么, 如果是对象,这里也是对象, 如果是文本,这里也是文本 |
#### 响应失败例子
```json
{"code":200,"data":{},"msg":"JS公共函数不存在"}
```
```json
{"code":200,"data":{},"msg":"异常:可能Hook函数传参或返回值类型错误,具体:SyntaxError: Unexpected token at the end: \u003cnil\u003e\n\tat parse (native)\n\tat WebApi_用户Id取详情 (\u003ceval\u003e:4:38(10))\n"}
```
```json
{"code":200,"data":{},"msg":"异常:可能Hook函数传参或返回值类型错误,具体:js引擎未返回报错信息"}
```
## 卡号
### 新制卡号
通过该WebApi制卡可以对接发卡平台
#### 请求例子
```http request
POST http://127.0.0.1:18888/WebApi/NewKa HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Id": 18,
    "Number": 2,
    "AdminNote": "测试WEB"
}
```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/NewKa |
| 请求方式 | POST |


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Id | int (整数型)|是|卡类id |
| Number | int (整数型)|是|生成数量 |
| AdminNote | string (文本型)|是|管理员备注 |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": [{
    "Id": 359,
    "Name": "18GS41nlZHjBBCHCM2IH8WqBG",
    "VipTime": 86400,
    "RMb": 1,
    "VipNumber": 1
  }, {
    "Id": 360,
    "Name": "1PDbU2i7lto5jdgcrXoLjUr5U",
    "VipTime": 86400,
    "RMb": 1,
    "VipNumber": 1
  }],
  "msg": "制卡成功"
}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | []Object (对象数组)|这里也是文本 |
|||
| Id | int (整数型)|卡号ID |
| Name |string (文本型)|卡号 |
| VipTime |int (整数型)卡号增加秒数或点数 |
| RMb |float (双精度小数)|卡号增加账号余额 |
| VipNumber |float (双精度小数)卡号增加账号积分 |


### 取卡号详情
获取卡号的详细信息
#### 请求例子
```http request
POST http://127.0.0.1:18888/WebApi/GetKaInfo HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "Name": "1PDbU2i7lto5jdgcrXoLjUr5U"
}
```
| 请求信息 |详细说明  |
|--| ----  |
| 请求路径 |域名/WebApi/GetKaInfo |
| 请求方式 | POST |


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Name | string (文本型)|是|要查询的卡号 |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": {
    "Id": 360,
    "AppId": 10001,
    "KaClassId": 18,
    "Name": "1PDbU2i7lto5jdgcrXoLjUr5U",
    "Status": 1,
    "RegisterUser": "admin",
    "RegisterTime": 1689040135,
    "AdminNote": "测试WEB",
    "AgentNote": "",
    "VipTime": 86400,
    "InviteCount": 1,
    "RMb": 1,
    "VipNumber": 1,
    "Money": 3,
    "AgentMoney": 3,
    "UserClassId": 21,
    "NoUserClass": 1,
    "KaType": 1,
    "MaxOnline": 1,
    "Num": 0,
    "NumMax": 1,
    "User": "",
    "UserTime": "",
    "InviteUser": ""
  },
  "msg": "获取成功"
}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | []Object (对象数组)|这里也是文本 |
|||
| Id | int (整数型)|卡号ID |
| AppId | int (整数型)|卡号所属应用AppId |
| KaClassId | int (整数型)|卡号所属卡类Id |
| Name |string (文本型)|卡号 |
| Status |int (整数型)卡号状态,1正常,2冻结 |
| RegisterUser |string (文本型)|制卡人用户名 |
| RegisterTime |int (整数型)|制卡时间戳 |
| AdminNote |string (文本型)|管理员备注 |
| AgentNote |string (文本型)|代理备注 |
| VipTime |int (整数型)|卡号增加秒数或点数 |
| InviteCount |int (整数型)|推荐人增加秒数或点数 |
| RMb |float (双精度小数)|增加余额数 |
| VipNumber |float (双精度小数)|增加积分数 |
| Money |float (双精度小数)|售价 |
| AgentMoney |float (双精度小数)|代理售价 |
| UserClassId |int (整数型)|转换后新用户类型 |
| NoUserClass |int (整数型)|新旧用户类型不同转换方式 |
| NoUserClass |int (整数型)|新旧用户类型不同转换方式 |
| KaType |int (整数型)|充值类型 |
| MaxOnline |int (整数型)|最大同时在线次数 |
| Num |int (整数型)|卡号已用次数 |
| NumMax |int (整数型)|卡号可用次数 |
| User |string (文本型)|已充值用户 |
| UserTime |int (整数型)|充值时间 |
| InviteUser |string (文本型)|已充值推荐人 |


## 订单
### 取支付订单状态
`1.0.148`版本增加
获取支付订单的支付状态,和其他信息,比如购卡订单, 返回购买的卡号  post或get都可以
支付宝同步回调地址支持变量`{OrderId}`订单id,`{OrderId2}`第三方订单id,`{User}`用户名,`{Type}`



这样就可以自己写个支付结果页,先带订单信息回调到自己的结果页,然后webApi请求获取支付状态
#### 请求例子
```http request
POST http://127.0.0.1:18888/WebApi/Pay/GetPayOrderStatus HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
Token: FROSRTAFZB0NKTKBJQQTCH35AEUHFWPS
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)

{
    "OrderId":"2023100622001414411410844851"
}
```
Get这样也可以
```http request
GET http://127.0.0.1:18888/WebApi/Pay/GetPayOrderStatus?OrderId=2023100622001414411410844851&Token=LAX6LCUN6KHBHYURTKHL2SM0QFIKMYG4 HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: no-cache
Content-Type: application/json
Pragma: no-cache
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64)
```


| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| OrderId | string (文本型)|是|要查询的订单号,或第三方订单号 |
#### 响应成功例子
```json
{
  "code": 10000,
  "data": {
    "KaName": "Y0197w1eHCD8ZzjYZiteiH5He",
    "Status": 3
  },
  "msg": "获取成功"
}
```
| 参数名 | 类型|详细说明  |
|--| ----  |----  |
| code | int (整数型)|响应状态码,具体请查看[状态码列表](/WebApi/WebApi.html#状态码列表),这里10000表示成功 |
| msg |string (文本型)|响应状态码,提示信息 |
| data | []Object (对象)| 数据对象|
|||
| Status | int (整数型)|订单状态 1 '等待支付' 2 '已付待充' 3 '充值成功' 4 退款中 5 ? 退款失败" : 6退款成功|
| KaName |string (文本型)|订单附属信息,如支付购卡订单, 该值为购买的卡号 |


