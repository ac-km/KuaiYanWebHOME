#  公共函数Api 文档
-深知每一个项目有不同需求,核心设计支持js,二次开发更简单,  
-通过公共函数,可以扩展更多功能,内置全局($XXX开头)变量,APi,与系统进行深度交互,  
-可以通过设置在线列表,永不注销管理员Token或代理Token,实现[公共函数内调用后台全部接口](/指南/常见问题.html#公共函数内token调用后台或代理接口功能)
## 全局变量
### $应用信息
```js
function 获取应用信息信息(形参) {
    //js内使用方式,仅可读取,写入无效
    $应用信息.AppId //  10001
    $应用信息.AppName //  演示对接账密限时Rsa交换密匙
    $应用信息.Status //  3
    return $应用信息
    /* $应用信息  返回对象会转JSON文本
    {
        "AppId": 10001,
        "AppName": "演示对接账密限时Rsa交换密匙",
        "Status": 3,
        "VipData": "{\n\"VipData\":\"这里的数据,只有登录成功并且账号会员不过期才会传输出去的数据\",\n\"VipData2\":\"这里的数据,只有登录成功并且账号会员不过期才会传输出去的数据\"\n}"
    }
     */
}
```
### $用户在线信息
```js
function 获取用户在线信息(形参) {
    //js内使用方式,仅可读取,写入无效
    $用户在线信息.User //  aaaaaa 用户名
    $用户在线信息.Key //  aaaaaa 用户的绑定信息
    $用户在线信息.Status //  1 用户状态,1 正常 2 冻结
    $用户在线信息.Tab //  用户动态标记,
    $用户在线信息.Uid //  用户Uid,账号的id,卡号登录的就是卡号的id
    return $用户在线信息
    /*   返回对象会转JSON文本
    {
        "Key": "aaaaaa",
        "Status": 1,
        "Tab": "AMD Ryzen 7 6800H with Radeon Graphics         |178BFBFF00A40F41",
        "Uid": 21,
        "User": "aaaaaa"
    }
     */
}
```
## 全局变量_hook专用
### $任务状态
`Hook任务执行入库前`和`Hook任务执行入库前` hook函数内可以使用,可以读取和修改任务状态
```js
function hook模板_任务创建入库前(任务JSON格式参数) {
    
    //例子随机任务状态成功的修改为任务状态失败
    if (Math.floor(Math.random() * 10) > 5 && $任务状态===3) {
        $任务状态 = 4
        return "状态被随机修改了,本来任务状态=3成功,但是现在被修改为状态=4 提交信息被修改为这一段废话"
    }
    return 任务JSON格式参数 //任务JSON格式文本型参数,可以在这里修改内容  然后返回
}
```
### $拦截原因 
`Hook任务执行入库前`和`Hook任务执行入库后`和`Hook任务执行执行前`和`Hook任务执行执行后`和`Api执行前后Hook函数`hook函数内可以使用,`如果值不为空,则任务拦截,响应拦截原因`
```js
function hook模板_任务创建入库前(任务JSON格式参数) {
    
    //例子随机拦截任务提交
    if (Math.floor(Math.random() * 10) > 5) {
        $拦截原因 = "拦截继续执行原因随机命中"
    }
    return 任务JSON格式参数 //任务JSON格式文本型参数,可以在这里修改内容  然后返回
}
```

### 请求信息 $Request
'1.0.118'版本增加 `Api执行前后Hook函数`专用  
可以获取到用户api大部分请求信息,
Api执行前hook可以在这里进行一些操作,如修改请求信息等等 会在Api执行前触发
```js
function hook模板_登录Api执行前Hook(JSON请求明文) {
    //这里的错误无法拦截,所以,如果js错误,可能会导致,用户返回"Api不存在" 测试时注意
    let 请求信息文本=JSON.stringify($Request)  //在 $Request对象里可以获取到 请求的大部分信息
    //{"Method":"POST","Url":{"Scheme":"","Opaque":"","User":null,"Host":"","Path":"/Api","RawPath":"","OmitHost":false,"ForceQuery":false,"RawQuery":"AppId=10002","Fragment":"","RawFragment":""},"Header":["Connection: Keep-Alive","Referer: http://127.0.0.1:18888/Api?AppId=10002","Content-Length: 467","Content-Type: application/x-www-form-urlencoded; Charset=UTF-8","Accept: */*","Accept-Language: zh-cn","User-Agent: Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)","Token: PNYDKXDHLORTNVGEEY99YYSPQGFLQF7L"],"Host":"127.0.0.1:18888","Body":[]}

    局_url = "https://www.baidu.com/"
    局_返回 = $api_网页访问_GET(局_url, 15, "")
    //{"StatusCode":200,"Headers":"Date: Sun, 21 May 2023 10:26:32 GMT\r\nContent-Length: 0\r\nContent-Type: application/x-www-form-urlencoded,\r\n","Cookies":"","Body":""}
    if (局_返回.Body !== "") {
        $拦截原因 = "百度可以访问,所以不能登录."    
    }
    //这里可以替换请求明文信息,可以实现很多功能,比如自写算法解密
    return JSON请求明文
}
```
Api执行后hook可以在这里进行一些操作,如修改响应信息等等 会在Api执行将要返回给用户时触发
```js
function hook模板_登录Api执行后Hook(JSON响应明文) {
    //{"Time":1697630688,"Status":200,"Msg":"百度可以访问,所以不能登录."}
    //{"Data":{"Key":"绑定信息","LoginIp":"127.0.0.1","LoginTime":1697630755,"OutUser":0,"RegisterTime":1696677905,"UserClassMark":2,"UserClassName":"vip2","VipNumber":0,"VipTime":1701300424},"Time":1697630755,"Status":73386,"Msg":""}

    let 局_返回信息 = JSON.parse(JSON响应明文) //把响应信息明文转换成对象,好操作
    if (局_返回信息.Status > 10000) {
        局_返回信息.Data.Key = "99999999" //返回的绑定信息被我修改了
    }
    JSON响应明文 = JSON.stringify(局_返回信息) //再把对象转换回明文字符串


    //局_url = "https://www.baidu.com/"
    //局_返回 = $api_网页访问_GET(局_url, 15, "")
    //局_返回 = $api_网页访问_POST(局_url, "api=123", 15, "")
    //{"StatusCode":200,"Headers":"Date: Sun, 21 May 2023 10:26:32 GMT\r\nContent-Length: 0\r\nContent-Type: application/x-www-form-urlencoded,\r\n","Cookies":"","Body":""}

    //这里可以替换响应的json信息文本, 如果想拦截直接替换为报错的json就可以了,注意状态码,和时间戳
    return JSON响应明文
}
```
## 全局函数 
### $api_用户Id取详情
```js
function 获取用户相关信息(形参) {
    //获取本次请求用户的详情
    var 局_用户信息 = $api_用户Id取详情($用户在线信息) //{"Id":21,"User":"aaaaaa","PassWord":"af15d5fdacd5fdfea300e88a8e253e82","Phone":"13109812593","Email":"1056795985@qq.com","Qq":"1059795985","SuperPassWord":"af15d5fdacd5fdfea300e88a8e253e82","Status":1,"Rmb":91.39,"Note":"","RealNameAttestation":"","Role":0,"UPAgentId":0,"AgentDiscount":0,"LoginAppid":10000,"LoginIp":"","LoginTime":1519454315,"RegisterIp":"113.235.144.55","RegisterTime":1519454315}

    //获取其他用户的详情
    $用户在线信息.Uid=2  //实际就用到了Uid,修改即可获取其他用户的信息
    var 局_用户信息 = $api_用户Id取详情($用户在线信息) 
    return 局_用户信息
/*    返回对象会转JSON文本
    {
        "Id": 21,
        "User": "aaaaaa",
        "PassWord": "af15d5fdacd5fdfe6666e88a8e253e82",
        "Phone": "13888888888",
        "Email": "1056795985@qq.com",
        "Qq": "1059795985",
        "SuperPassWord": "af15d5fdacd5fdfea300e88a8e266666",
        "Status": 1,
        "Rmb": 91.39,
        "Note": "",
        "RealNameAttestation": "",
        "Role": 0,
        "UPAgentId": 0,
        "AgentDiscount": 0,
        "LoginAppid": 10000,
        "LoginIp": "",
        "LoginTime": 1519454315,
        "RegisterIp": "113.235.144.55",
        "RegisterTime": 1519454315
    }
*/
}
```
### $api_取软件用户详情
'1.0.92'版本增加
```js
function 获取用户相关信息(形参) {
    //获取本次请求用户的详情
    var 局_用户信息 = $api_取软件用户详情($用户在线信息)
    return 局_用户信息
/*    返回对象会转JSON文本
{
    "Id": 4,
    "Uid": 8,
    "Status": 1,
    "Key": "17821712363",
    "VipTime": 1726406089,
    "VipNumber": 99.65,
    "Note": "",
    "MaxOnline": 1,
    "UserClassId": 0,
    "RegisterTime": 1694870089
}
*/
}
```
### $api_用户Id余额增减
`$api_用户Id余额增减($用户在线信息 , 增减值 float64, 原因 string)`
`1.0.94`版本之后 函数名由`$api_用户Id余额增减`,修改为`$api_用户Id增减余额`
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| $用户在线信息 | Object (对象)|是|直接输入全局变量就行 $用户在线信息 |
| 增减值 | float64 (双精度小数)|是|增减值,正值增加,负值减少,减少无法减少到0以下 ,增加无限制, |
| 原因 |string(文本型)|是| 日志记录原因 |


返回为对象`{IsOk: true, Err: ""}`
| 参数名 | 类型|是否必须|详细说明  |
|----| ----  |------- |----  |
| IsOk | boom (逻辑型)|是|增减成功返回ture(真),失败返回false(假) |
| Err|string(文本型)|是| 失败原因文本,一般就是余额不足减少失败 |

```js
function 用户余额增减案例(JSON形参文本) {
    局_结果 = $api_用户Id余额增减($用户在线信息, -0.01, "测试公共函数扣余额")
    
    if (局_结果.IsOk){
        $用户在线信息.Uid=2  //实际就用到了Uid,修改即可操作其他用户
        局_结果 = $api_用户Id余额增减($用户在线信息, 0.01, "测试增加余额其他用户余额")
    }
    
    return 局_结果 // {IsOk: true, Err: ""}
}
```
### $api_用户Id积分增减
`$api_用户Id积分增减(AppId int, $用户在线信息 , 增减值 float64, 原因 string)`

| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| AppId | int (整数型)|是|应用ID |
| $用户在线信息 | Object (对象)|是|直接输入全局变量就行 $用户在线信息 |
| 增减值 | float64 (双精度小数)|是|增减值,正值增加,负值减少,减少无法减少到0以下 ,增加无限制, |
| 原因 |string(文本型)|是| 日志记录原因 |


返回为对象`{IsOk: true, Err: ""}`
| 参数名 | 类型|是否必须|详细说明  |
|----| ----  |------- |----  |
| IsOk | boom (逻辑型)|是|增减成功返回ture(真),失败返回false(假) |
| Err|string(文本型)|是| 失败原因文本,一般就是余额不足减少失败 |

```js
function 用户积分增减案例(JSON形参文本) {
    局_结果 = $api_用户Id积分增减($应用信息.AppId,$用户在线信息, 0.01, "测试公共函数扣积分")
    return 局_结果 // {IsOk: true, Err: ""}
}
```
### $api_用户Id增减时间点数
`$api_用户Id增减时间点数(AppId int,$用户在线信息 ,增减值 int, 原因 string)`
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| AppId | int (整数型)|是|应用ID |
| $用户在线信息 | Object (对象)|是|直接输入全局变量就行 $用户在线信息 |
| 增减值 | int (整数型)|是|增减值,正值增加,负值减少,减少无法减少到0以下 ,增加无限制, 时间单位秒 |
| 原因 |string(文本型)|是| 日志记录原因 |


返回为对象`{IsOk: true, Err: ""}`
| 参数名 | 类型|是否必须|详细说明  |
|----| ----  |------- |----  |
| IsOk | boom (逻辑型)|是|增减成功返回ture(真),失败返回false(假) |
| Err|string(文本型)|是| 失败原因文本,一般就是时间或点数不足减少失败 |

```js
function 用户增减时间点数增减案例(JSON形参文本) {
    局_结果 = $api_用户Id增减时间点数($应用信息.AppId,$用户在线信息, -10, "测试公共函数扣时间点数")
    
    if (局_结果.IsOk){
        $用户在线信息.Uid=2  //实际就用到了Uid,修改即可操作其他用户
        局_结果 = $api_用户Id增减时间点数($应用信息.AppId,$用户在线信息, 10, "测试增加其他用户")
    }
    
    return 局_结果 // {IsOk: true, Err: ""}
}
```


### $api_读公共变量

`$api_读公共变量(变量名 string)`
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| 变量名 |string(文本型)|是| 公共变量名称 |

返回为字符串文本 `飞鸟快验管理系统`

```js
function 读写公共变量案例(JSON形参文本) {

    var 待写入变量 = $api_读公共变量("系统名称")    //直接返回文本型变量值
    
    var 局_逻辑 = $api_置公共变量("系统名称", 待写入变量 + "追加1")   //返回逻辑性值

    return 局_逻辑
}
```
### $api_置公共变量

`$api_置公共变量(变量名 string,值 string)`
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| 变量名 |string(文本型)|是| 公共变量名称  如果变量不存在,只会直接创建|
| 值 |string(文本型)|是| 要置入的公共变量值 |

返回为`boom(逻辑型)` 置入成功返回 `ture`,失败返回`false`
```js
function 读写公共变量案例(JSON形参文本) {

    var 待写入变量 = $api_读公共变量("系统名称")    //直接返回文本型变量值
    
    var 局_逻辑 = $api_置公共变量("系统名称", 待写入变量 + "追加1")   //返回逻辑性值

    return 局_逻辑
}
```

### $api_网页访问_GET
`$api_网页访问_GET(Url, 协议头一行一个, Cookies string, 超时秒数 int, 代理ip string)`
```js
function 测试网页访问(JSON形参文本) {

    局_url = "https://www.baidu.com/sugrec?&prod=pc_his&from=pc_web&json=1&sid=38516_36555_38613_38538_38595_38581_36803_38485_38637_26350_38621_38663&hisdata=%5B%7B%22time%22%3A1675596837%2C%22kw%22%3A%22python%E8%BF%90%E8%A1%8C%E6%97%B6%E4%BF%AE%E6%94%B9%E4%BB%A3%E7%A0%81%22%7D%2C%7B%22time%22%3A1675605796%2C%22kw%22%3A%22%E7%86%8A%E7%8C%AB%208.23%20%E6%BA%90%E7%A0%81%22%2C%22fq%22%3A2%7D%2C%7B%22time%22%3A1675609301%2C%22kw%22%3A%22go%20%E4%BC%98%E7%A7%80%E9%A1%B9%E7%9B%AE%22%7D%2C%7B%22time%22%3A1675671958%2C%22kw%22%3A%22win11%20%E7%AA%97%E5%8F%A3%E6%97%A0%E6%B3%95%E6%8B%96%E6%96%87%E4%BB%B6%22%7D%2C%7B%22time%22%3A1675673946%2C%22kw%22%3A%22win11%20%E6%97%A0%E6%B3%95%E6%8B%96%E6%94%BE%22%2C%22fq%22%3A3%7D%2C%7B%22time%22%3A1676041744%2C%22kw%22%3A%22ns%20retroarch%20%E6%9A%82%E5%81%9C%22%7D%2C%7B%22time%22%3A1676042251%2C%22kw%22%3A%22ns%20retroarch%20%E6%B2%A1%E6%9C%89%E8%AE%BE%E7%BD%AE%22%7D%2C%7B%22time%22%3A1676096636%2C%22kw%22%3A%22gin%20%E5%92%8C%20beego%22%7D%2C%7B%22time%22%3A1676126858%2C%22kw%22%3A%22ruby%E5%92%8Cgo%22%7D%2C%7B%22time%22%3A1676177555%2C%22kw%22%3A%22%E7%A7%9F%E5%8F%B7%E7%8E%A9%E7%BD%91%E6%98%93%E4%B8%8A%E5%8F%B7%E5%99%A8%22%7D%5D&_t=1684664739701&req=2&csor=0"

    返回对象 = $api_网页访问_GET(局_url,"", "",15, "")
    //返回对象 = $api_网页访问_POST(局_url, "api=123", 15, "")
    //{"StatusCode":200,"Headers":"Date: Sun, 21 May 2023 10:26:32 GMT\r\nContent-Length: 0\r\nContent-Type: application/x-www-form-urlencoded,\r\n","Cookies":"","Body":""}
    return 返回对象
}
```
### $api_网页访问_POST
`$api_网页访问_POST(Url, post,协议头一行一个, Cookies string, 超时秒数 int, 代理ip string)`

| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| Url |string(文本型)|是| 请求的网址,http或https开头如 |
| post |string(文本型)|是| 请求提交数据 |
| 协议头一行一个 |string(文本型)|是| 协议头一行一个 |
| Cookies |string(文本型)|是| Cookies格式`nouid=64996eeb0c70c4341; pv_st=B8DED36081B274871BF6; pv_n=B8;`  |
| 超时秒数 |int(整数型)|是| 推荐15秒|
| 代理ip |string(文本型)|是| 代理ip格式`127.0.0.1:10888` |

返回为`boom(逻辑型)` 置入成功返回 `ture`,失败返回`false`
```js
function 测试网页访问(JSON形参文本) {

    局_url = "https://www.baidu.com/"
    //返回对象 = $api_网页访问_GET(局_url,"", "",15, "")
    返回对象 = $api_网页访问_POST(局_url,"api=123", "", "", 15, "")
    
    return 返回对象
}
```
### $api_置动态标记

`$api_置动态标记(在线信息 DB.DB_LinksToken, 新动态标签 string) `
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| $用户在线信息 | Object (对象)|是|直接输入全局变量就行 $用户在线信息 |
| 值 |string(文本型)|是| 要置入的公共变量值 |

返回为`boom(逻辑型)` 置入成功返回 `ture`,失败返回`false`
```js
function 读写置动态标记案例(JSON形参文本) {

    //$用户在线信息.Tab  这个就是当前在线信息的动态标记
    
    var 局_逻辑 =  $api_置动态标记($用户在线信息, $用户在线信息.Tab + "追加文本")   //返回逻辑型值

    return 局_逻辑
}
```
### $api_执行SQL查询

`$api_执行SQL查询(SQL语句 string) `
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| SQL语句 | string (文本型)|是|只能输入查询的SQL语句(`SELECT`), |


返回为对象`{IsOk: true, Err: ""}`
| 参数名 | 类型|是否必须|详细说明  |
|----| ----  |------- |----  |
| IsOk | boom (逻辑型)|是|成功返回ture(真),失败返回false(假) |
| Err|string(文本型)|是| 如果成功就是查询结果json,失败就是错误失败 |
```js
function 执行SQL查询测试(JSON形参文本) {
    
    var 局_结果对象 = $api_执行SQL查询(" SELECT * FROM `db_public_js`")   //获取公共函数数据库全部信息
    if (局_结果对象.isOk) {
        //这里说明查询成功了,
        return 局_结果对象.Err
    }

    return "查询失败了"
}
```
### $api_执行SQL功能

`$api_执行SQL功能(SQL语句 string) `
| 参数名 | 类型|是否必须|详细说明  |
|--| ----  |----  |----  |
| SQL语句 | string (文本型)|是|只能输入非查询的SQL语句(增删改等等), |


返回为对象`{IsOk: true, Err: ""}`
| 参数名 | 类型|是否必须|详细说明  |
|----| ----  |------- |----  |
| IsOk | boom (逻辑型)|是|成功返回ture(真),失败返回false(假) |
| Err|string(文本型)|是| 如果成功值就是影响行数,失败就是错误信息 |
```js
function 执行SQL功能测试(JSON形参文本) {

    var 局_结果对象 = $api_执行SQL功能("UPDATE db_public_js SET Type=Type+1 WHERE  Id=11") //获取公共函数数据库全部信息
    if (局_结果对象.isOk) {
        //这里说明成功了,
        let 影响行数 = Number(局_结果对象.Err)
        return 影响行数 //返回影响行数
    }

    return 局_结果对象.Err
}
```
### $程序_延时
'1.0.92'版本增加
发现js同步延迟还挺麻烦,增加这个函数,效仿精易.例子 查看$api_任务池_任务查询 的例子
### $api_任务池_任务创建
'1.0.92'版本增加
例子和查询在一起
### $api_任务池_任务查询
'1.0.92'版本增加
```js
function 任务池创建查询例子(形参) {
    let 任务类型ID = 1
    let 结果 = $api_任务池_任务创建($用户在线信息, 任务类型ID, JSON形参文本)
    //{"IsOk":true,"Err":"","Data":{"TaskUuid":"1fb701a9-05c5-442a-8bcc-34bda07050ae"}}
    
    if (结果.IsOk) {
        let 局_任务对象 = 结果.Data
        let 任务结果
        for (let i = 0; i < 3; i++) {
            $程序_延时(5000); // 等待1秒
            任务结果 = $api_任务池_任务查询(局_任务对象.TaskUuid)
            if (任务结果.Data.Status !== 1 && 任务结果.Data.Status !== 2 ) { //不是刚创建, 也不是处理中,跳出循环
                break
            }
        }
        //{"IsOk":true,"Err":"","Data":{"ReturnData":"","Status":1,"TimeEnd":0,"TimeStart":1695016978}}
        if (任务结果.Data.Status === 3){
            // 如果是成功,直接返回
            return {
                Code: 1,
                Msg: "ok",
                recognition: 任务结果.Data.ReturnData,
            }
        }
    }
    return {
        Code: -1,
        Msg: "失败",
    }
}

const js对象_通用返回 = {
    IsOk: false,
    Err: "",
    Data: {}
};
```
