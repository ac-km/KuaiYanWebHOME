---
home: true

heroImage: /images/logo4.png

[//]: # (这里的缩进一定要处理 actions 下边的 不然无法显示报错)
actions:
  - text: 快速入门
    link: /指南/快速入门.html
    type: primary
  - text: 常见问题
    link: /指南/常见问题.html
    type: secondary
features:
  - title: 简洁至上
    details: 人性化简洁配置,更易对接,注重用户体验细节处理,。
  - title: Vue 驱动
    details: 享受 Vue 的前端体验，WEB网页管理,无需客户端,有浏览器就ok。
  - title: Go高性能
    details: Go后端 语言级别支持多线程多协程开发,榨干每一分服务器资源。
  - title: 用户体验
    details: 2小时内维护bug,24小时内评估新功能需求,不在睡觉,就在服务,客户即上帝。
  - title: 二次开发
    details: 深知每一个项目有不同需求,核心设计支持js,二次开发更简单
  - title: 数据安全
    details: 用户数据存储您自己服务器数据库中,随时迁移转换数据无惧服务商跑路,丢失客户数据
---


<html>
  <footer>
    <div style="margin: 0 auto; text-align: center">
      <!-- 其他底部内容 -->
      <a style="font-size: 4px ;color: rgba(45,75,74,0.57)" href="https://beian.miit.gov.cn/" target="_blank">备案号</a>
      <a style="font-size: 4px ;color: rgba(45,75,74,0.57)" href="https://beian.miit.gov.cn/" target="_blank">辽ICP备15007240号-4</a>
    </div>
  </footer>
</html>
