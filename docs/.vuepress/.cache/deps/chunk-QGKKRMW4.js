import {
  ks,
  mt,
  oh
} from "./chunk-LI566XZP.js";

// node_modules/.pnpm/registry.npmmirror.com+mermaid@10.2.3/node_modules/mermaid/dist/selectAll-92b901d1.js
function r(e) {
  return typeof e == "string" ? new mt([document.querySelectorAll(e)], [document.documentElement]) : new mt([oh(e)], ks);
}

export {
  r
};
//# sourceMappingURL=chunk-QGKKRMW4.js.map
