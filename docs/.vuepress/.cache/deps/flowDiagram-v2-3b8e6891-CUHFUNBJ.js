import {
  he,
  ye
} from "./chunk-EBEIPWCO.js";
import "./chunk-RUFZS5WP.js";
import {
  Je,
  et
} from "./chunk-WBWV5F24.js";
import "./chunk-UB2XU6JW.js";
import "./chunk-5VPUAFUT.js";
import "./chunk-EUWLPZG2.js";
import "./chunk-TU5FCJ46.js";
import "./chunk-WQDB67JC.js";
import "./chunk-GR7HS7ZY.js";
import "./chunk-HGUEGZCV.js";
import "./chunk-QGKKRMW4.js";
import {
  qg
} from "./chunk-LI566XZP.js";

// node_modules/.pnpm/registry.npmmirror.com+mermaid@10.2.3/node_modules/mermaid/dist/flowDiagram-v2-3b8e6891.js
var A = {
  parser: et,
  db: Je,
  renderer: he,
  styles: ye,
  init: (r) => {
    r.flowchart || (r.flowchart = {}), r.flowchart.arrowMarkerAbsolute = r.arrowMarkerAbsolute, qg({ flowchart: { arrowMarkerAbsolute: r.arrowMarkerAbsolute } }), he.setConf(r.flowchart), Je.clear(), Je.setGen("gen-2");
  }
};
export {
  A as diagram
};
//# sourceMappingURL=flowDiagram-v2-3b8e6891-CUHFUNBJ.js.map
