import {defaultTheme, defineUserConfig} from "vuepress";
import {mdEnhancePlugin} from "vuepress-plugin-md-enhance";
import codeCopyPlugin from '@snippetors/vuepress-plugin-code-copy'

import {registerComponentsPlugin,} from '@vuepress/plugin-register-components'



export default defineUserConfig({

    plugins: [
/*        searchPlugin({
            // 搜索框配置项  https://v2.vuepress.vuejs.org/zh/reference/plugin/search.html

        }),*/
        codeCopyPlugin({         // 插件参考 https://snippetors.github.io/plugins/vuepress-plugin-code-copy.html
            successText: "已复制",
            staticIcon: false,           // 是否常显 复制按钮
        }),
        mdEnhancePlugin({  // 插件参考 https://plugin-md-enhance.vuejs.press/zh/
            // 你的选项
            mermaid: true,
            demo: true,
        }),
/*        registerComponentsPlugin({
          //  componentsDir: './components', //组件库目录
            //componentsDir: path.resolve(__dirname, '../../', 'components'), // 自动注册全局组件
        }),*/

    ],


    lang: 'zh-CN',
    title: '飞鸟快验',
    description: `高性能,易管理,快对接,更自由,开源通用WEB管理网络验证后台 `,


    // 站点基础路径值
    base: '/',

    //浏览器标签页上的图标  引入百度统计js
    head: [['link', {rel: 'icon', href: 'images/logo4.png'}], ['script', {}, `
  var _hmt = _hmt || [];
  (function() {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?b9de9bc00fdb1d736ab89e414ee465d3";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
  })();
  `]],

    // configure default theme
    theme: defaultTheme({
        logo: 'images/logo4.png',
        docsDir: 'docs',
        repo: 'https://gitee.com/anyueyinluo/KuaiYanGo2',
//上方导航栏右侧的可下拉路由菜单
        navbar: [
            // 菜单组
            {
                text: '指南',
                children: [
                    '/指南/快速入门.html',
                    '/指南/常见问题.html',
                    '/指南/状态码查询.html',
                    '/指南/3级代理系统.html',
                    '/指南/在线支付支付宝对接教程.html',
                    '/指南/更新日志.html'
                ],
            }, {
                text: '演示站',
                link: 'https://demo.fnkuaiyan.cn/Admin',
                target: '_blank',
                rel: 'noopener noreferrer'
            }, {
                text: '用户Api',
                link: '/用户Api/用户Api.html',
            },
            // 单个菜单


            {
                text: 'WebApi',
                link: '/WebApi/WebApi',
            },
            // 单个菜单
            {
                text: '公共函数Api',
                link: '/PublicJs/公共函数Api.html',
            },

            /*      // 菜单组
                  {
                    text: '用户Api',
                    children: ['/用户Api/GetTask.html', '/用户Api/SetTask.html'],
                  },*/
            // 字符串 - 页面文件路径
            //'/WebApi/README.md',
        ],
    }),
})
