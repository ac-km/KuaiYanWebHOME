import {defineClientConfig} from '@vuepress/client';
// 引入自定义注册全局组件
import Button666 from "./components/Button666.vue";
import Timeline from "./components/Timeline.vue";
import UpdateTimeline from "./components/UpdateTimeline.vue";

export default defineClientConfig({
    enhance({app}) {
        //手动注册vue组件到全局
        app.component('Timeline', Timeline)
        app.component('Button666', Button666)
        app.component('UpdateTimeline', UpdateTimeline)
    },
})
