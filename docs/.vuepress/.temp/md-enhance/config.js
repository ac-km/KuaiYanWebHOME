import { defineClientConfig } from "@vuepress/client";
import Mermaid from "E:/yun/xuhaonan/project/TY通用后台管理系统/官网静态WEB/node_modules/.pnpm/registry.npmmirror.com+vuepress-plugin-md-enhance@2.0.0-beta.228_vuepress@2.0.0-beta.63/node_modules/vuepress-plugin-md-enhance/lib/client/components/Mermaid.js";
import { injectMermaidConfig } from "E:/yun/xuhaonan/project/TY通用后台管理系统/官网静态WEB/node_modules/.pnpm/registry.npmmirror.com+vuepress-plugin-md-enhance@2.0.0-beta.228_vuepress@2.0.0-beta.63/node_modules/vuepress-plugin-md-enhance/lib/client//index.js";

export default defineClientConfig({
  enhance: ({ app }) => {
    injectMermaidConfig(app);
    app.component("Mermaid", Mermaid);
  },
  setup: () => {

  }
});
