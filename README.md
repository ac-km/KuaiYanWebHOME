# 打包命令
```
vuepress build  docs
```

# 打包报错 不是内部或外部命令，也不是可运行的程序 或批处理文件
因为缓存文件有问题,所以无法编译,用下边的先清除缓存
```text
 vuepress build    --clean-cache 
```

上面不行就只能 
删除 node_modules 文件夹后 
执行  pnpm install  重新安装后
pnpm run docs:build  再打包
